# Copyright 2014-16 Akretion - Alexis de Lattre <alexis.delattre@akretion.com>
# Copyright 2014 Serv. Tecnol. Avanzados - Pedro M. Baeza
# Copyright 2021 Tecnativa - Víctor Martínez
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from flectra import api, fields, models


class AccountMove(models.Model):
    _inherit = "account.move"

    @api.depends("partner_id", "payment_mode_id")
    def _compute_partner_bank(self):
        res = super(AccountMove, self)._compute_partner_bank()
        for move in self:
            if move.move_type == 'out_invoice' and not move.partner_bank_id:
                move.partner_bank_id = move.company_id.partner_id.bank_ids[:1]
        return res
