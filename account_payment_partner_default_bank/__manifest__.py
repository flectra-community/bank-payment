{
    "name": "Account Payment Partner Default Bank",
    "version": "2.0.1.5.0",
    "category": "Banking addons",
    "license": "AGPL-3",
    "summary": "Use first bank account of company as default",
    "author": "Jamotion GmbH",
    "website": "https://gitlab.com/flectra-community/bank-payment",
    "depends": ["account_payment_partner"],
    "data": [],
    "demo": [],
    "installable": True,
}
