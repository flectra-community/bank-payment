# Copyright 2016 Akretion (<http://www.akretion.com>).
# Copyright 2017 Tecnativa - Vicent Cubells
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Account Payment Purchase Stock",
    "version": "2.0.1.0.1",
    "category": "Banking addons",
    "license": "AGPL-3",
    "summary": "Integrate Account Payment Purchase with Stock",
    "author": "Akretion, Tecnativa, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/bank-payment",
    "depends": ["account_payment_purchase", "purchase_stock"],
    "installable": True,
    "auto_install": True,
}
