# Flectra Community / bank-payment

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[account_payment_order_return](account_payment_order_return/) | 2.0.1.0.2| Account Payment Order Return
[account_payment_order_vendor_email](account_payment_order_vendor_email/) | 2.0.1.0.0| Account Payment Order Email
[account_payment_purchase_stock](account_payment_purchase_stock/) | 2.0.1.0.1| Integrate Account Payment Purchase with Stock
[account_payment_partner](account_payment_partner/) | 2.0.1.5.0| Adds payment mode on partners and invoices
[account_payment_order](account_payment_order/) | 2.0.1.8.3| Account Payment Order
[account_payment_purchase](account_payment_purchase/) | 2.0.1.0.2| Adds Bank Account and Payment Mode on Purchase Orders
[account_payment_mode](account_payment_mode/) | 2.0.1.1.0| Account Payment Mode
[account_banking_sepa_direct_debit](account_banking_sepa_direct_debit/) | 2.0.1.3.3| Create SEPA files for Direct Debit
[account_banking_sepa_credit_transfer](account_banking_sepa_credit_transfer/) | 2.0.1.2.0| Create SEPA XML files for Credit Transfers
[account_banking_mandate_sale](account_banking_mandate_sale/) | 2.0.1.0.0| Adds mandates on sale orders
[account_invoice_select_for_payment](account_invoice_select_for_payment/) | 2.0.1.0.0| Account Invoice Select for Payment
[account_banking_pain_base](account_banking_pain_base/) | 2.0.1.0.1| Base module for PAIN file generation
[account_payment_sale](account_payment_sale/) | 2.0.1.0.0| Adds payment mode on sale orders
[account_banking_mandate](account_banking_mandate/) | 2.0.1.2.0| Banking mandates


